import { IonListHeader, IonLabel, IonItem, IonBadge } from "@ionic/react";
import { pipe } from "fp-ts/lib/function";
import { prettyFormatPoken } from "../core/business/poken_business";
import { PokensPrettyWallet } from "../types/pokens";
import ChainPokensOthers from "./ChainPokensOthers";
import * as E from "fp-ts/Either";

type DisplayPokensProps = {
  addr: string;
  pokens: PokensPrettyWallet;
};

// show pokens of both chain : ERC and BSC
const DisplayPokens: React.FC<DisplayPokensProps> = ({
  addr,
  pokens,
}: DisplayPokensProps) => {
  return (
    <>
      <IonListHeader className="ion-margin-top">
        <IonLabel className="pk-bold">On address {addr}</IonLabel>
      </IonListHeader>
      <IonItem className="ion-margin-top">
        <IonLabel>ERC Chain</IonLabel>
      </IonItem>
      <IonItem lines="none">
        <IonLabel>Poken</IonLabel>
        <IonBadge slot="end" color="primary">
          {pipe(
            pokens.erc.pknErc,
            E.fold(
              (err) => "Error",
              (pk) => `${prettyFormatPoken(pk)} PKN`
            )
          )}
        </IonBadge>
      </IonItem>
      <ChainPokensOthers pokens={pokens.erc} pokenColor="tertiary" />
      <IonItem className="ion-margin-top">
        <IonLabel>BSC Chain</IonLabel>
      </IonItem>
      <IonItem lines="none">
        <IonLabel>Pokens</IonLabel>
        <IonBadge slot="end" color="primary">
          {pipe(
            pokens.bsc.pknBsc,
            E.fold(
              (err) => "Error",
              (pk) => `${prettyFormatPoken(pk)} PKN`
            )
          )}
        </IonBadge>
      </IonItem>
      <ChainPokensOthers pokens={pokens.bsc} pokenColor="secondary" />
    </>
  );
};

export default DisplayPokens;
