import { IonItem, IonLabel, IonBadge } from "@ionic/react";
import { pipe } from "fp-ts/lib/function";
import { prettyFormatPoken } from "../core/business/poken_business";
import { PokensChainWallet } from "../types/pokens";
import "./ChainPokensOthers.css";
import * as E from "fp-ts/Either";
import { NFP_DECIMAL } from "../core/business/poken_constants";
interface ChainPokensProps {
  pokens: PokensChainWallet;
  pokenColor: string; // primary, secondary, ...
}

// Show the pokens of a chain (ex : ethereum, bsc)
const ChainPokensOthers: React.FC<ChainPokensProps> = ({
  pokens,
  pokenColor,
}: ChainPokensProps) => {
  return (
    <>
      <IonItem lines="none">
        <IonLabel>Stacked</IonLabel>
        <IonBadge slot="end" color={pokenColor}>
          {pipe(
            pokens.stacked,
            E.fold(
              (err) => "Error",
              (pk) => `${prettyFormatPoken(pk)} PKN`
            )
          )}
        </IonBadge>
      </IonItem>
      <IonItem lines="none">
        <IonLabel>Pooled</IonLabel>
        <IonBadge slot="end" color={pokenColor}>
          {pipe(
            pokens.pooled,
            E.fold(
              (err) => "Error",
              (pk) => `${prettyFormatPoken(pk)} PKN`
            )
          )}
        </IonBadge>
      </IonItem>
      <IonItem lines="none">
        <IonLabel>NFP</IonLabel>
        <IonBadge slot="end" color={pokenColor}>
          {pipe(
            pokens.nfp,
            E.fold(
              (err) => "Error",
              (pk) => `${prettyFormatPoken(pk, NFP_DECIMAL)}`
            )
          )}
        </IonBadge>
      </IonItem>
    </>
  );
};

export default ChainPokensOthers;
