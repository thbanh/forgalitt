import { INFURA_PROJ_ID, INFURA_PROJ_SECRET } from "../secret";

import {
  IonButton,
  IonContent,
  IonHeader,
  IonLabel,
  IonPage,
  IonTitle,
  IonToolbar,
  IonInput,
  IonItem,
  IonNote,
  useIonLoading,
} from "@ionic/react";

import { useEffect, useState } from "react";

import { BSC_NETWORK_MAINNET_CHAIN_ID } from "../core/blockchain/bsc";
import { ERC_NETWORK_MAINNET } from "../core/blockchain/erc";
import { PokenAppConfig, userPokens } from "../core/business/poken_business";
import { POKEN_CONTRACTS } from "../core/business/poken_constants";
import { PokensPrettyWallet } from "../types/pokens";

import "./Pokens.css";

import { useForm } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";
import DisplayPokens from "../components/DisplayPokens";
import { PokenAddress } from "../core/blockchain/access";

const USER_ADDR_EX = [
  "0xe3285793164de00394ce7fc1d451b4f42693553b",
  "0xa7365864af5838cc31b8513dd1d685eeb77d216d",
  "0xd55514630dcdc66f97e1a325905180128182ba7a",
  "0x16EED4e8E12282320f5cc74b07907F922431782b",
];

// Get all the pokens of an address
const getPokens = async (addr: string) => {
  const config: PokenAppConfig = {
    ercChainAccess: {
      provider: {
        infuraProjectSecret: INFURA_PROJ_SECRET,
        infuraProjectId: INFURA_PROJ_ID,
      },
      network: ERC_NETWORK_MAINNET,
    },
    bscChainAccess: {
      provider: {
        binanceUrl: "https://bsc-dataseed.binance.org/",
      },
      network: BSC_NETWORK_MAINNET_CHAIN_ID,
    },
    pokenContracts: POKEN_CONTRACTS,
  };

  const pokens = await userPokens(config, addr);

  return { ...pokens };
};

// Pokens page
const Pokens: React.FC = () => {
  // Ionic
  const [present, dismiss] = useIonLoading();

  // React state
  const [pokens, setPokens] = useState<PokensPrettyWallet | undefined>(
    undefined
  );
  const [userAddr, setUserAddr] = useState<PokenAddress | undefined>(undefined);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  // React Effect
  useEffect(() => {
    isLoading ? present("Getting Pokens data") : dismiss();
  }, [isLoading, dismiss, present]);

  // Form management
  type PokenDataForm = {
    addr: string;
  };

  const {
    // control,
    // setValue,
    // getValues,
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<PokenDataForm>({
    defaultValues: {
      addr: "",
    },
  });

  const onSubmit = (data: PokenDataForm) => {
    setIsLoading(true);
    (async () => {
      const pokens = await getPokens(data.addr);
      setIsLoading(false);
      setPokens(pokens);
      setUserAddr(data.addr);
    })();
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Your Pokens</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent className="ion-padding">
        <form onSubmit={handleSubmit(onSubmit)}>
          <IonItem>
            <IonLabel className="pk-bold">Address</IonLabel>
            <IonInput
              placeholder={USER_ADDR_EX[0]}
              {...register("addr", {
                required: "An address is a required to show your pokens",
                pattern: {
                  value: /^0x[a-fA-F0-9]{40}$/i,
                  message: "Invalid address",
                },
              })}
            />
          </IonItem>
          <ErrorMessage
            errors={errors}
            name="addr"
            as={<IonNote color="danger" className="pk-block" />}
          />
          <IonButton
            fill="outline"
            className="ion-margin-vertical"
            type="submit"
          >
            Show my Pokens
          </IonButton>

          <p>
            <IonNote className="pk-block">Try those addresses</IonNote>
            {USER_ADDR_EX.map((a) => (
              <IonButton
                fill="clear"
                size="small"
                onClick={() => onSubmit({ addr: a })}
                key={a}
              >
                {a}
              </IonButton>
            ))}
          </p>
        </form>

        {/* Show the token when pokens an address are available */}
        {pokens && userAddr && (
          <DisplayPokens pokens={pokens} addr={userAddr} />
        )}
      </IonContent>
    </IonPage>
  );
};

export default Pokens;
