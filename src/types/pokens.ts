import * as E from "fp-ts/Either";
import { PokenBigNumber, PokenError } from "../core/business/poken_constants";

export type PokensChainWallet = {
  pknErc: E.Either<PokenError, PokenBigNumber>;
  pknBsc: E.Either<PokenError, PokenBigNumber>;
  stacked: E.Either<PokenError, PokenBigNumber>;
  pooled: E.Either<PokenError, PokenBigNumber>;
  nfp: E.Either<PokenError, PokenBigNumber>;
};

export type PokensPrettyWallet = {
  erc: PokensChainWallet;
  bsc: PokensChainWallet;
};
