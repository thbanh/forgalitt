import * as E from "fp-ts/lib/Either";
import { ADAPTER_ETHERSJS, getAdapter } from "../adapter/adapter";
import {} from "../adapter/ethersjs";
import {
  ChainAccessBsc,
  ChainAccessErc,
  PokenAddress,
} from "../blockchain/access";
import {
  PokenBigNumber,
  PokenContracts,
  PokenError,
  POKEN_DECIMAL,
} from "./poken_constants";

export type PokenAppConfig = {
  ercChainAccess: ChainAccessErc;
  bscChainAccess: ChainAccessBsc;
  pokenContracts: PokenContracts;
};

export type ChainUserPokens = {
  pknErc: E.Either<PokenError, PokenBigNumber>;
  pknBsc: E.Either<PokenError, PokenBigNumber>;
  stacked: E.Either<PokenError, PokenBigNumber>;
  pooled: E.Either<PokenError, PokenBigNumber>;
  nfp: E.Either<PokenError, PokenBigNumber>;
};

export type AllUserPokens = {
  erc: ChainUserPokens;
  bsc: ChainUserPokens;
};

// Get BSC and ERC pokens
export const userPokens = async (
  config: PokenAppConfig,
  userAddr: PokenAddress
): Promise<AllUserPokens> => {
  const { userPokens } = getAdapter(ADAPTER_ETHERSJS);
  const ercPokens = await userPokens(
    userAddr,
    config.ercChainAccess,
    config.pokenContracts
  );

  const bscPokens = await userPokens(
    userAddr,
    config.bscChainAccess,
    config.pokenContracts
  );

  return { erc: ercPokens, bsc: bscPokens };
};

// expose a format poken function that hides the implementation
export const prettyFormatPoken = (
  number: PokenBigNumber,
  decimal = POKEN_DECIMAL
) => {
  const { formatPokens } = getAdapter(ADAPTER_ETHERSJS);

  return formatPokens(number, decimal);
};
