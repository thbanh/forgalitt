import { BigNumber as BigNumberEtherJsImpl } from "ethers";
import { PokenAddress } from "../blockchain/access";
import {
  ContractAbi,
  STACKED_CONTRACT_ABI,
  POOLED_CONTRACT_ABI,
  NFP_CONTRACT_ABI,
  POKEN_CONTRACT_ABI,
} from "./poken_contract_abi";

export const POKEN_DECIMAL = 18;
export const NFP_DECIMAL = 0;

export type PokenContracts = {
  pknErc: { address: PokenAddress; abi: ContractAbi };
  pknBsc: { address: PokenAddress; abi: ContractAbi };
  stacked: { address: PokenAddress; abi: ContractAbi };
  pooled: { address: PokenAddress; abi: ContractAbi };
  nfp: { address: PokenAddress; abi: ContractAbi };
};

export const POKEN_CONTRACTS: PokenContracts = {
  pknErc: {
    address: "0xdf09a216fac5adc3e640db418c0b956076509503",
    abi: POKEN_CONTRACT_ABI,
  },
  pknBsc: {
    address: "0x4b5decb9327b4d511a58137a1ade61434aacdd43",
    abi: POKEN_CONTRACT_ABI,
  },

  stacked: {
    address: "0x6aee45f9ff5cee2ad72aec36236b1bb77703d18b",
    abi: STACKED_CONTRACT_ABI,
  },
  pooled: {
    address: "0x3B04bf46Fb84F33702095df461e5666D02E6Bc48",
    abi: POOLED_CONTRACT_ABI,
  },
  nfp: {
    address: "0xD8B793B352a73a4eCF4651EF3DD17D6393a61F8e",
    abi: NFP_CONTRACT_ABI,
  },
};

export type PokenBigNumber = BigNumberEtherJsImpl;

export const NO_PROVIDER_IDENTIFIED = "noProviderIdentified";
export const PKN_STACKED_BY = "pknStackedBy";
export const PKN_POOLED_BY_USER = "pknPooledByUser";
export const BALANCE_OF_NFP = "balanceOfNfp";
export const PKN_ERC_BALANCE = "balanceOfPknErc";
export const PKN_BSC_BALANCE = "balanceOfPknBsc";

export type PokenError = {
  errorType:
    | typeof NO_PROVIDER_IDENTIFIED
    | typeof PKN_STACKED_BY
    | typeof PKN_POOLED_BY_USER
    | typeof BALANCE_OF_NFP
    | typeof PKN_BSC_BALANCE
    | typeof PKN_ERC_BALANCE;
  errorCatch: Error;
};
