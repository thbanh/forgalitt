To run the app :
- Install Ionic
- Rename the file src/secret_example.ts to src/secret.ts
    - Put the projectId and ProjectSecret of your Infura account
- run ionic serve

Known bugs :
- with the address 0x16EED4e8E12282320f5cc74b07907F9224317800, we have an error detected but the loading indicator does not disapper

Directory structure :
- pages/ : Ionic pages
- components/ : React component used inside the pages
- core/ : non GUI related stuff
- core/business/ : about poken business
- core/blockchain : about blockchain
- core/adapter : lib used to interact with the blockchains
